from __future__ import print_function, unicode_literals, absolute_import

import asyncore
import socket
import subprocess
import re
import syslog

LOOKINGGLASS = 1
PING = 1
TRACEROUTE = 1

ip4pattern = re.compile(r'(([01]?[0-9]?[0-9]|2[0-4][0-9]|2[5][0-5])\.){3}([01]?[0-9]?[0-9]|2[0-4][0-9]|2[5][0-5])')

class POPHandler(asyncore.dispatcher_with_send):

    def handle_read(self):
        data = self.recv(8192)
        if data:
            self.handle_data(data)

    def handle_data(self, data):
        try:
            (action, arguments) = data.split('|', 1)
        except ValueError:
            self.close()

        if action == "ping" and PING == 1:
            self.handle_ping(arguments)

        elif action == "trace" and TRACEROUTE == 1:
            self.handle_trace(arguments)

        elif action == "bgproute" and LOOKINGGLASS == 1:
            self.handle_bgp(arguments)

        else:
            self.close()

    def handle_ping(self, target):
        self._exec(['ping', '-c4', target.strip()])

    def handle_trace(self, target):
        self._exec(['traceroute', target.strip()])

    def handle_bgp(self, target):
        ipaddr = target.strip()

        if ip4pattern.match(ipaddr):
            self._exec(['birdc', 'show', 'route', 'for', target.strip(), 'table', 'ebgp', 'all'])
        else:
            self.send('Go away!')
            self.close()

    def _exec(self, cmd):
        syslog.syslog('Executing: %s' % cmd)
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        for line in iter(proc.stdout.readline, ''):
            self.send(line)
        _ = subprocess.communicate()
        self.close()

class POPServer(asyncore.dispatcher):

    def __init__(self, host, port):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((host, port))
        self.listen(5)

    def handle_accept(self):
        pair = self.accept()
        if pair is not None:
            sock, addr = pair
            handler = POPHandler(sock)
