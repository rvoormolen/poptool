from __future__ import print_function, unicode_literals, absolute_import

from . import poptool

## Metadata
__author__ = r'Rick Voormolen'
__email__ = r'rick@voormolen.org'
__version__ = r'1.0'

## Main loop
def main():
    server = poptool.POPServer('0.0.0.0', 42129)
    poptool.asyncore.loop()
