#!/usr/bin/env python
try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup, find_packages

import os, sys
if os.getuid() > 0:
    print("Use sudo to install this package, we need root privileges")
    sys.exit(1)


import poptool
import subprocess
import platform


config = {
    'description': 'poptool',
    'author': poptool.__author__,
    'author_email': poptool.__email__,
    'version': poptool.__version__,
    'install_requires': [],
    'packages': find_packages(),
    'name': 'poptool',
    'data_files': [ ],
    'entry_points': {
        'console_scripts': [
            'poptool = poptool:main',
        ]
    }
}

try:
    git_version = subprocess.check_output("git rev-list HEAD --count".split(" "), stderr=subprocess.PIPE).strip()
except:
    ## No GIT
    pass
else:
    ## Adjust to GIT version numbering
    config['version'] = "%0.1f-%s" % ((float(poptool.__version__) - 0.1), git_version)

## Detect init system
detect_platform = platform.dist()
# ('debian', '8.1', '')
init_system = 'systemv'

if detect_platform[0] == 'debian' and float(detect_platform[1]) >= 8:
    init_system = 'systemd'
    config['data_files'].append(('/etc/systemd/system', [ 'contrib/poptool.service' ],))

setup(**config)
